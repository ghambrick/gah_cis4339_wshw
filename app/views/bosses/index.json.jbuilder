json.array!(@bosses) do |boss|
  json.extract! boss, :id, :name
  json.url boss_url(boss, format: :json)
end
