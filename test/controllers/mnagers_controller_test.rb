require 'test_helper'

class MnagersControllerTest < ActionController::TestCase
  setup do
    @mnager = mnagers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:mnagers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create mnager" do
    assert_difference('Mnager.count') do
      post :create, mnager: { name: @mnager.name }
    end

    assert_redirected_to mnager_path(assigns(:mnager))
  end

  test "should show mnager" do
    get :show, id: @mnager
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @mnager
    assert_response :success
  end

  test "should update mnager" do
    patch :update, id: @mnager, mnager: { name: @mnager.name }
    assert_redirected_to mnager_path(assigns(:mnager))
  end

  test "should destroy mnager" do
    assert_difference('Mnager.count', -1) do
      delete :destroy, id: @mnager
    end

    assert_redirected_to mnagers_path
  end
end
