class CreateMnagers < ActiveRecord::Migration
  def change
    create_table :mnagers do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
